import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AppComponent} from "./app.component";
import {PersonalDetailsComponent} from "./personal-details/personal-details.component";
import {RegistrationComponent} from "./registration/registration.component";
import {SystemPersonalInformationComponent} from "./system-personal-information/system-personal-information.component";
import {UserPIComponent} from "./user-pi/user-pi.component";

const routes: Routes = [
  { path: 'getPatientDetails', component: RegistrationComponent },
  { path: 'PersonalDetails', component: PersonalDetailsComponent },
  { path: 'personalInformation', component: SystemPersonalInformationComponent },
  { path: 'Patient-ID', component: UserPIComponent },

  { path: '',   redirectTo: '/getPatientDetails', pathMatch: 'full' }, // redirect to `first-component`
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
