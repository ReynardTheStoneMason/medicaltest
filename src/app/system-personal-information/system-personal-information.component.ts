import { Component } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {map, Observable, startWith} from "rxjs";
import {FormControl} from "@angular/forms";
import { HttpClient } from '@angular/common/http';
import {MatTableDataSource} from "@angular/material/table";

export interface User {
  name: string;
}

@Component({
  selector: 'app-system-personal-information',
  templateUrl: './system-personal-information.component.html',
  styleUrls: ['./system-personal-information.component.css']
})
export class SystemPersonalInformationComponent {

  categoryOptions:Object=[]
  SystemCategoryDataset:any=[]
  categoryValueDataset:any=[]
  category_field=""
  field:string=""
  validation:string=""
  serialNo:string=""
  dataSource: any;
  dataSource2:any;
  constructor(
    private route: ActivatedRoute,
    private http: HttpClient
  ) {}

  ngOnInit() {
    this.getAll()
    this.getAllSPI()

    console.log("DATASOURCE", this.dataSource)
  }


  displayedColumns: string[] = ['SERIAl_NO', 'CATEGORY', 'SELECT', ];
  displayedColumns2: string[] = ['SERIAL_NO', 'CATEGORY', 'FIELD', 'VALIDATION', ];

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  applyFilter2(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource2.filter = filterValue.trim().toLowerCase();
  }

  selectCategory(element:any){
    this.category_field = element.CATEGORY

  }

  saveCategoryValue(){
    this.insertCategoriesSPI()
    // this.categoryValueDataset.push({
    //   category_field: this.category_field,
    //   field:this.field,
    //   validation: this.validation,
    //   serialNo: this.serialNo
    //
    // })
    // this.dataSource2 = new MatTableDataSource(this.categoryValueDataset)
  }

  public getAll(){
    console.log("first");

    this.categoryOptions=[]


    this.http.post(`http://localhost:3000/getAllData`, {}).subscribe(data => {
      console.log("PostREquenst")
      console.log("This is Data",data);
      this.SystemCategoryDataset = data;
      // console.log(this.runningNumber[0].number);
//      this.runningNumber = this.runningNumber[0].number;
      console.log("this",this.SystemCategoryDataset);
      console.log(this.SystemCategoryDataset)
      this.dataSource = new MatTableDataSource(this.SystemCategoryDataset);
      console.log("DataSource",this.dataSource)

    });



  }

  insertCategoriesSPI(){
    this.http.post('http://localhost:3000/insertCategoriesSPI', {category: this.category_field, validation: this.validation,
      serialNo: this.serialNo, field:this.field }).subscribe(data => {
      console.log("Pushing Category to Database", data)

      this.getAll()
      this.getAllSPI()

    })
  }


  getAllSPI(){
    this.http.post(`http://localhost:3000/getAllDataSPI`, {}).subscribe(data => {
      console.log("PostREquenst")
      console.log("This is Data",data);
      this.categoryValueDataset = data;
      // console.log(this.runningNumber[0].number);
//      this.runningNumber = this.runningNumber[0].number;
      console.log("this SPI",this.categoryValueDataset);
      console.log(this.categoryValueDataset)
      this.dataSource2 = new MatTableDataSource(this.categoryValueDataset);
      console.log("DataSource2",this.dataSource2)

    });
  }


}
