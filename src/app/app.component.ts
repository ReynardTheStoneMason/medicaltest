import { Component } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'medicalTest';

  newPatient: boolean = false;
  existingPatient: boolean = false;
  DOB:Date = new Date()
  dateandtime: Date = new Date()
  uuid:string="";
  showTable :boolean=false
  fileToUpload: File | null = null;


  patientDataset: any[] = []

  firstname:string=""
  lastname:string=""
  middlename  :string=""
  patient_ID:string=""




  showExistingPatient(){
    this.existingPatient = true
    this.newPatient = false

  }
  showNewPatient(){
    this.existingPatient = false
    this.newPatient = true
  }
  constructor(private router: Router) {

  }

  saveNewPatient(){
    this.showTable = true;
    this.dateandtime = new Date()
    this.patient_ID = new Date().getUTCMilliseconds().toString()
    // this.router.navigate(['/PersonalDetails'], { state: { name: 'bar' } });
    this.showTable = true;

    this.patientDataset.push({
      firsname:this.firstname,
      middlename: this.middlename,
      lastname:this.lastname,
      DOB:this.DOB,
      patientID: this.patient_ID})

  }

  saveExistingPatient(){
    this.showTable = true;
    this.dateandtime = new Date();
    // this.router.navigate(['/PersonalDetails'], { state: { name: 'bar' } });
    this.showTable = true;


    this.patientDataset.push({
      firsname:this.firstname,
      middlename: this.middlename,
      lastname:this.lastname,
      DOB:this.DOB,
      patientID: this.patient_ID
    })



  }

}
