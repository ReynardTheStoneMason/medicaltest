import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPIComponent } from './user-pi.component';

describe('UserPIComponent', () => {
  let component: UserPIComponent;
  let fixture: ComponentFixture<UserPIComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserPIComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserPIComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
