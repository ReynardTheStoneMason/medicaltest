import { Component } from '@angular/core';
import {Router} from "@angular/router";
import {FormControl} from "@angular/forms";
import {User} from "../system-personal-information/system-personal-information.component";
import {map, Observable, startWith} from "rxjs";
import { NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';
import {  OperatorFunction } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { FormsModule } from '@angular/forms';
import { JsonPipe } from '@angular/common';
import {MatTableDataSource} from "@angular/material/table";
import {HttpClient} from "@angular/common/http";
@Component({
  selector: 'app-user-pi',
  templateUrl: './user-pi.component.html',
  styleUrls: ['./user-pi.component.css']
})
export class UserPIComponent {
  patient_ID : string;
  firstname: string;
  lastname: string;
  categoryList:any=[]
  fieldList:any=[]
  cardDataSet:any=[];

  public model: any;

  formatter = (result: string) => result.toUpperCase();

  // search: OperatorFunction<string, readonly string[]> = (text$: Observable<string>) =>
  //   text$.pipe(
  //     debounceTime(200),
  //     distinctUntilChanged(),
  //     map((term) =>
  //       term === '' ? [] : this.states.filter((v) => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10),
  //     ),
  //   );

  constructor(private router: Router, public http:HttpClient) {
    this.patient_ID = this.router.getCurrentNavigation()!.extras.state!['patient_ID']!
    this.firstname = this.router.getCurrentNavigation()!.extras.state!['firstname']!
    this.lastname = this.router.getCurrentNavigation()!.extras.state!['lastname']!

    console.log(
      ); // should log out 'bayr'
  }


  myControl = new FormControl<string | User>('');
  options: User[] = [{name: 'Mary'}, {name: 'Shelley'}, {name: 'Igor'}];
  filteredOptions: Observable<User[]>;

  ngOnInit() {
    // this.addEmptyField()
    this.getAllSPI()
    this.getAllUPI()
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => {
        const name = typeof value === 'string' ? value : value?.name;
        return name ? this._filter(name as string) : this.options.slice();
      }),
    );
  }

  displayFn(user: User): string {
    return user && user.name ? user.name : '';
  }

  private _filter(name: string): User[] {
    const filterValue = name.toLowerCase();

    return this.options.filter(option => option.name.toLowerCase().includes(filterValue));
  }

   addEmptyField(){
    this.cardDataSet.push({
      _id: "",
      PATIENT_ID: this.patient_ID,
      CATEGORY: "",
      FIELD:"",
      VALUE:"",
      FROMDATE:"",
      TODATE:""
    })
  }



  upsertRecord(item: any){
      console.log(item)
      this.http.post('http://localhost:3000/insertCategoriesUPI', {_id:item._id, Patient_ID: item.PATIENT_ID, FirstName : item.FIRSTNAME,
        LastName : item.LASTNAME, fromdate:item.FROMDATE, todate: item.TODATE, category: item.CATEGORY.key, field: item.FIELD, value: item.VALUE  }).subscribe(data => {
        console.log("Pushing Category to Database", data)

        this.getAllUPI()

    });
  }

  getAllUPI(){
    this.http.post(`http://localhost:3000/getAllDataUPI`, {}).subscribe((data: any) => {
      console.log("PostREquenst")
      console.log("This is UPI: ",data);
      this.cardDataSet = data;

      console.log(data)


      // console.log(this.runningNumber[0].number);
//      this.runningNumber = this.runningNumber[0].number;
//       console.log("this SPI",this.categoryValueDataset);
//       console.log(this.categoryValueDataset)
//       this.dataSource2 = new MatTableDataSource(this.categoryValueDataset);
//       console.log("DataSource2",this.dataSource2)

    });
    this.addEmptyField()
  }




  getAllSPI(){
    this.http.post(`http://localhost:3000/getAllDataSPI`, {}).subscribe((data: any) => {
      console.log("PostREquenst")
      console.log("This is Data",data);
      this.categoryList= []

      for (var i of data){
        this.categoryList.push({key:i.CATEGORY, value: []})
      }

      for(var i of data){
        for(var j of this.categoryList){
          if (j.key == i.CATEGORY){
            j.value.push( i.FIELD)
          }
        }
      }
      alert(this.categoryList)
      console.log(this.categoryList)





    });
  }
}


