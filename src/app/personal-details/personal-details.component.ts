import {Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {HttpClient} from "@angular/common/http";
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'app-personal-details',
  templateUrl: './personal-details.component.html',
  styleUrls: ['./personal-details.component.css']
})
export class PersonalDetailsComponent implements OnInit{

  categoryValueDataset:any=[]
  category_field:string=""
  value:string=""
  serialNo:string=""

  dataSource:any = []

  displayedColumns: string[] = ['SERIAl_NO', 'CATEGORY', 'VALUE', ];

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  constructor(
    private route: ActivatedRoute,
    private http: HttpClient
  ) {}

  ngOnInit() {

    this.categoryValueDataset = this.getAllCatgories()

    this.route.queryParams.subscribe(params => {
      this.uuid = params['name'];
    });
  }

  uuid:string =""

  saveCategoryValue(){


    this.insertCategories();
  }

  getAllCatgories(){
    this.http.post('http://localhost:3000/getAllCategories', {}).subscribe(data => {
      console.log("Categories List", data)
      this.categoryValueDataset = data;
      console.log("categoryValueDataset", this.categoryValueDataset)
      this.dataSource = new MatTableDataSource(this.categoryValueDataset)
    })
  }


  insertCategories(){
    this.http.post('http://localhost:3000/insertCategories', {category: this.category_field, value: this.value,
      serialNo: this.serialNo }).subscribe(data => {
      console.log("Pushing Category to Database", data)

      this.getAllCatgories()

    })
  }




}
