import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {MatTableDataSource} from "@angular/material/table";
@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit{
  title = 'medicalTest';

  newPatient: boolean = false;
  existingPatient: boolean = false;
  DOB:Date = new Date()
  dateandtime: Date = new Date()
  uuid:string="";
  showTable :boolean=false
  fileToUpload: File | null = null;
  dataSource:any =[]
  showUSERPIButton: boolean = false;
  patientDataset: any = []

  firstname:string=""
  lastname:string=""
  middlename  :string=""
  patient_ID:string=""


  displayedColumns: string[] = ['PATIENT_ID','FIRSTNAME' , 'MIDDLENAME', 'LASTNAME','DOB', 'SELECT'];

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  showExistingPatient(){
    this.existingPatient = true
    this.newPatient = false

  }
  showNewPatient(){
    this.existingPatient = false
    this.newPatient = true
  }
  constructor(private router: Router, private http:HttpClient) {

  }

  saveNewPatient(){
    this.showTable = true;
    this.dateandtime = new Date()
    this.patient_ID = new Date().getUTCMilliseconds().toString()
    // this.router.navigate(['/PersonalDetails'], { state: { name: 'bar' } });
    this.showTable = true;

    this.insertCategories()
  }

  saveExistingPatient(){
    this.showTable = true;
    this.dateandtime = new Date();
    // this.router.navigate(['/PersonalDetails'], { state: { name: 'bar' } });
    this.showTable = true;

    this.insertCategories()





  }

  getAllCatgories(){
    this.http.post('http://localhost:3000/getAllDataSPD', {}).subscribe(data => {
      console.log("Categories List", data)
      this.patientDataset = data;
      console.log("patient Dataset", this.patientDataset)
      this.dataSource = new MatTableDataSource(this.patientDataset)

    })
  }


  insertCategories(){
    this.http.post('http://localhost:3000/insertCategoriesSPD', {Patient_ID: this.patient_ID, FirstName : this.firstname,
      LastName : this.lastname, MiddleName:this.middlename, DOB: this.DOB  }).subscribe(data => {
      console.log("Pushing Category to Database", data)

      this.getAllCatgories()

    })
  }

  ngOnInit(): void {

    this.getAllCatgories()
  }


  showUSERPI(){
    this.showUSERPIButton = true;


  }

  enterUSERPI(){
    this.router.navigate(['Patient-ID'], { state: { patient_ID: this.patient_ID, firstname:this.firstname,lastname:this.lastname,middlename:this.middlename } });
  }
  selectPatient(element: any){
    this.showExistingPatient()
    this.patient_ID = element.PATIENT_ID
    this.firstname = element.FIRSTNAME
    this.middlename = element.MIDDLENAME
    this.lastname = element.LASTNAME
    this.DOB = element.DOB
    this.showUSERPI()
  }

}

