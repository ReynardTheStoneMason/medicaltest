import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoriesDialogBoxComponent } from './categories-dialog-box.component';

describe('CategoriesDialogBoxComponent', () => {
  let component: CategoriesDialogBoxComponent;
  let fixture: ComponentFixture<CategoriesDialogBoxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CategoriesDialogBoxComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CategoriesDialogBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
