var express = require('express');
var router = express.Router();
var ObjectId = require('mongodb').ObjectID;
const MongoClient = require('mongodb').MongoClient;
const uri = "mongodb+srv://root:root@connectplacecluster.0gdr9.gcp.mongodb.net/?retryWrites=true&w=majority";

const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });


const database = client.db("MedicalDatabase");

const cors = require('cors');

client.connect(err => {
  const collection = client.db("").collections("appaProject");
  // database = client.db();
  console.log("Database connection done.");

});

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/getAllCategories', function(req, res, next) {
  database.collection("SYSTEM_CAT").find().toArray(function (error, data) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    console.log(data);
    res.send(data)

  });
});

router.post('/insertCategories', function(req, res, next) {
  console.log(req.body)
  database.collection("SYSTEM_CAT").insertOne({CATEGORY: req.body["category"],SERIAl_NO: req.body["serialNo"],VALUE: req.body["value"]}, function (err, result) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    if (err) {
      res.send({status: 400,message: "Error inserting document!"});
    } else {
      console.log(`Added a new match with id ${result.insertedId}`);
      res.send({status: 204, id: result.insertedId});
    }
  });

});


router.post('/getAllData', function(req, res, next) {
  database.collection("SYSTEM_CAT").find().toArray(function (error, data) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    console.log("GetAllDataREqyestr", data);
    res.send(data)

  });
});


router.post('/getAllDataUPI', function(req, res, next) {
  database.collection("USER_PI").find().toArray(function (error, data) {
    console.log(data)
    res.setHeader("Access-Control-Allow-Origin", "*");
    console.log("USERPI DATA REQUEST", data);
    res.send(data)

  });
});



router.post('/getAllDataSPD', function(req, res, next) {
  database.collection("SYSTEM_PD").find().toArray(function (error, data) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    console.log("GetAllDataREqyestr", data);
    res.send(data)

  });

});

router.post('/getAllDataSPI', function(req, res, next) {
  database.collection("SYSTEM_PI").find().toArray(function (error, data) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    console.log("GetAllDataREqyestr", data);
    res.send(data)

  });
});

router.post('/insertCategoriesSPI', function(req, res, next) {
  console.log(req.body)

  database.collection("SYSTEM_PI").insertOne({CATEGORY: req.body["category"],SERIAL_NO: req.body["serialNo"],FIELD: req.body["field"],VALIDATION: req.body["validation"]}, function (err, result) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    if (err) {
      res.send({status: 400,message: "Error inserting document!"});
    } else {
      console.log(`Added a new match with id ${result.insertedId}`);
      res.send({status: 204, id: result.insertedId});
    }
  });

});

router.post('/insertCategoriesSPD', function(req, res, next) {
  console.log(req.body)
  database.collection("SYSTEM_PD").insertOne({PATIENT_ID: req.body["Patient_ID"],FIRSTNAME: req.body["FirstName"],MIDDLENAME: req.body["MiddleName"],LASTNAME: req.body["LastName"],DOB: req.body["DOB"]}, function (err, result) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    if (err) {
      res.send({status: 400,message: "Error inserting document!"});
    } else {
      console.log(`Added a new match with id ${result.insertedId}`);
      res.send({status: 204, id: result.insertedId});
    }
  });

});


router.post('/insertCategoriesUPI', function(req, res, next) {
  console.log(req.body)
  var options = { upsert: true };
  // console.log(res.body._id)

  if ( req.body._id === ""){
    console.log("EMPTY")
    database.collection("USER_PI").insertOne({
      PATIENT_ID: req.body["Patient_ID"],
      FIRSTNAME: req.body["FirstName"],
      LASTNAME: req.body["LastName"],
      FROMDATE: req.body["fromdate"],
      TODATE: req.body["todate"],
      VALUE:  req.body["value"],
      CATEGORY: req.body["category"],
      FIELD: req.body["field"],
    }, function (err, result) {
      res.setHeader("Access-Control-Allow-Origin", "*");
      if (err) {
        res.send({status: 400,message: "Error inserting document!"});
      } else {
        console.log(`Added a new match with id ${result.insertedId}`);
        res.send({status: 204, id: result.insertedId});
      }
    });

  }
  else {
    // console.log(res.body._id)
    database.collection("USER_PI").updateOne( {_id: ObjectId(res.body['_id'])},
      { "$set" : {
          PATIENT_ID: req.body["Patient_ID"],
          FIRSTNAME: req.body["FirstName"],
          LASTNAME: req.body["LastName"],
          FROMDATE: req.body["fromdate"],
          TODATE: req.body["todate"],
          VALUE:  req.body["value"],
          CATEGORY: req.body["category"],
          FIELD: req.body["field"],
        }
      }, function (err, result) {
        res.setHeader("Access-Control-Allow-Origin", "*");
        if (err) {
          res.send({status: 400,message: "Error Updated document!"});
        } else {
          console.log(`Updated a new ID with id ${result.insertedId}`);
          res.send({status: 204, id: result.insertedId});
        }
      });

  }



});



module.exports = router;
